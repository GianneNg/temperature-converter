package com.example.giann.temperatureconverter1;

import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;

public class Converter extends AppCompatActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_converter);

        final EditText tempe=(EditText)findViewById(R.id.tempbox);
        final RadioButton cel2far=(RadioButton) findViewById(R.id.c2f);
        final RadioButton far2cel =(RadioButton) findViewById(R.id.f2c);
        final Button con = (Button) findViewById(R.id.computebtn);


        con.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                double ans = new Double(tempe.getText().toString());
                if (far2cel.isChecked())
                {
                    ans = TempConverter.FahrenheitToCelsius(ans);
                }
                else
                    ans = TempConverter.CelsiusToFahrenheit(ans);
                tempe.setText(new Double(ans).toString());
            }
        });
    }
}

