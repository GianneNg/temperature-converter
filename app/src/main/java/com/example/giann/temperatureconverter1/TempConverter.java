package com.example.giann.temperatureconverter1;

/**
 * Created by giann on 14/07/2017.
 */

public class TempConverter {
    public static double CelsiusToFahrenheit (double f){
        return (f-32) * 5/9;
    }

    public static double FahrenheitToCelsius (double c){
        return 32 + c * 9/5;
    }
}
